FROM golang:1.13.1
WORKDIR /go/src/demo-webapp
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build .

FROM scratch
COPY --from=0 /go/src/demo-webapp/demo-webapp /demo-webapp
COPY templates /templates
ENTRYPOINT ["/demo-webapp"]
