package main

import (
    "context"
    "fmt"
    "html/template"
    "log"
    "net/http"
    "os"
    "os/signal"
    "syscall"
)

const (
    version = "v2.0"
)

func main() {
    log.Println("Starting helloworld application...")

    t, err := template.ParseFiles("templates/index.tmpl")
    if err != nil {
        log.Println(err)
    }
    
    data := struct {
        Version string
    }{
        Version: version,
    }

    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        err := t.Execute(w, data)
        if err != nil {
            log.Println(err)
        }
    })

    http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
        w.WriteHeader(200)
    })

    http.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, version)
    })

    s := http.Server{Addr: ":8080"}
    go func() {
        log.Fatal(s.ListenAndServe())
    }()

    signalChan := make(chan os.Signal, 1)
    signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
    <-signalChan

    log.Println("Shutdown signal recieved, existing...")

    s.Shutdown(context.Background())
}
